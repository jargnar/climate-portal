DROP DATABASE rainfed;
DROP USER rainfed;
CREATE DATABASE rainfed;
CREATE USER rainfed WITH PASSWORD 'rainfed';
GRANT ALL PRIVILEGES ON DATABASE rainfed TO rainfed;