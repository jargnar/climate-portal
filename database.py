'''
The MIT License (MIT)
Copyright (c) 2016 Suhas SG

postgres=# CREATE DATABASE rainfed;
postgres=# CREATE USER rainfed WITH PASSWORD 'rainfed';
postgres=# GRANT ALL PRIVILEGES ON DATABASE rainfed TO rainfed;
'''
import yaml
import sqlalchemy
import pandas as pd
from orderedattrdict.yamlutils import AttrDictYAMLLoader


def connect(cfg):
    '''Returns a connection and a metadata object'''
    con = sqlalchemy.create_engine(cfg.dburl, client_encoding='utf8')
    meta = sqlalchemy.MetaData(bind=con, reflect=True)
    return con, meta


def setup(cfg):
    con, meta = connect(cfg)
    for name, table in cfg.schema.iteritems():
        fields = []
        for field, spec in table.iteritems():
            fields.append(sqlalchemy.Column(field, **spec.column))
        sqlalchemy.Table(name, meta, *fields, extend_existing=True)

    meta.create_all(con)


def sample(cfg):
    con, meta = connect(cfg)
    districts = meta.tables['c_districts']
    clause = districts.select().where(districts.c.district_code == 522)
    df = pd.read_sql(clause, con)
    del df['year']
    print df.mean().to_dict()


if __name__ == '__main__':
    cfg = yaml.load(open('config.yaml'), Loader=AttrDictYAMLLoader)
    setup(cfg)
    # sample(cfg)
