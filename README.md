India rainfed portal: Climate analytics
---

## API Overview

```
## Climate summaries

    /state?id=
    /district?id=
    /subdistrict?id=
    /village?id=


    {
        "state_code": "...",
        "metric_a": "...",
        "metric_b": "...",
        ...
    }

## Time series for a metric

    /state?id=&metric=
    /district?id=&metric=
    /subdistrict?id=&metric=
    /village?id=&metric=

    {
        "state_code": "...",
        "data": [
            { "metric": "...", "year": "..." },
            ...
        ]
    }
    
```


## Database setup

- Install postgresql, and python dependencies (sqlalchemy, psycopg2)
- Ensure that you can login to the postgres console by running `psql -U postgres`
- Pull the latest code
- Run `psql -U postgres -f setup.sql`
- Run `python database.py`
- Download climate portal data
- Login to postgres prompt as "rainfed" user by running `psql -U rainfed`, password is "rainfed"
- Run the following commands to populate data
    ```
    \copy c_states from '/path/to/c_states.csv' delimiter ',' csv header;
    \copy c_districts from '/path/to/c_districts.csv' delimiter ',' csv header;
    \copy c_subdistricts from '/path/to/c_subdistricts.csv' delimiter ',' csv header;
    \copy c_villages from '/path/to/c_villages.csv' delimiter ',' csv header;
    ```

- Create indices to speed retrieval. After loading all data, run the below command (this will take a while)
    ```
    CREATE INDEX c_villages_index ON c_villages (village_code);
    CREATE INDEX c_subdistrict_index ON c_subdistricts (subdistrict_code);
    CREATE INDEX c_district_index ON c_district (district_code);
    ```


